﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCore.Repository.Entity
{
    public class Room {
        public Guid Id { get; set; }
        public RoomType RoomType { get; set; }
        public Guid RoleTypeId { get; set; }
        public User User { get; set; }
        public Guid UserId { get; set; }
        public bool Status { get; set; }
	public int Test {get;set;}
    }
}
