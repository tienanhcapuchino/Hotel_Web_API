﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCore.Repository.Entity
{
    public class Bill
    {
        public Guid Id { get; set; }
        public User User { get; set; }
        public Guid UserId { get; set; }
        public PaymentMode ModePay { get; set; }
        public DateTime PaymentDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public bool Status { get; set; }
        public string? Note { get; set; }
    }
    public enum PaymentMode
    {
        COD = 1,
        InternentBanking = 2,
        Momo = 3,
        MasterCard = 4,
    }
}
