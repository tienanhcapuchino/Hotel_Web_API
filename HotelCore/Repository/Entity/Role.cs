﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCore.Repository.Entity
{
    public class Role
    {
        public Guid Id { get; set; }
        public RoleType Title { get; set; }
        public string Description { get; set; }
    }
    public enum RoleType
    {
        Admin = 1,
        Customer = 2,
    }
}
