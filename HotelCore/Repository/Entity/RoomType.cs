﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCore.Repository.Entity
{
    public class RoomType
    {
        public Guid Id { get; set; }
        public string Image { get; set; }
        public double Price { get; set; }
        public int NumberPersons { get; set; }
        public int NumberRooms { get; set; }
    }
}
