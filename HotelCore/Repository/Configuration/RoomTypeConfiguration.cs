﻿using HotelCore.Repository.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCore.Repository.Configuration
{
    public class RoomTypeConfiguration : IEntityTypeConfiguration<RoomType>
    {
        public void Configure(EntityTypeBuilder<RoomType> builder)
        {
            builder.ToTable("RoomType");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.NumberPersons);
            builder.Property(x => x.NumberRooms);
            builder.Property(x => x.Price);
            builder.Property(x => x.Image);
        }
    }
}
