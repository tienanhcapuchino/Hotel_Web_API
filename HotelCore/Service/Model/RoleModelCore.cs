﻿using HotelCore.Repository.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCore.Service.Model
{
    public class RoleModelCore
    {
        public RoleType Title { get; set; }
        public string Description { get; set; }
    }
}
