﻿using HotelCore.Repository.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCore.Service.Model
{
    public class UserModelCore
    {

    }
    public class UserLoginCore
    {
        public string? Username { get; set; }
        public string? Password { get; set; }
    }
    public class UserRegisterCore
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool Gender { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string RePassword { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime DepartureDate { get; set; }
        public RoleType Role { get; set; }
        public DateTime DateBirth { get; set; }
    }
}
