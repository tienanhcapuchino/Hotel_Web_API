﻿using HotelCore.Common;
using HotelCore.Repository.Entity;
using HotelCore.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCore.Service.Interface
{
    public interface IRoleService
    {
        Task<ResultCustom> AddNewRole(RoleModelCore roleModel);
        List<Role> GetAllRoles();
        Task<ResultCustom> Update(Guid roleId, RoleModelCore item);
        Task<Role?> GetRoleByTitle(RoleType title);
    }
}
