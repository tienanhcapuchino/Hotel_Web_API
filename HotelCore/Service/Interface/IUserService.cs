﻿using HotelCore.Common;
using HotelCore.Repository.Entity;
using HotelCore.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCore.Service.Interface
{
    public interface IUserService
    {
        Task<ResultCustom> Login(UserLoginCore model);
        Task<ResultCustom> Register(UserRegisterCore model);
        Task<bool> DeactivateUser(Guid userId);
        Task<User?> GetUserByUsername(string username);
    }
}
