﻿using HotelCore.Common;
using HotelCore.Repository.DataContext;
using HotelCore.Repository.Entity;
using HotelCore.Service.Interface;
using HotelCore.Service.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCore.Service.Service
{
    public class UserService : IUserService
    {
        private readonly HotelDbContext _repository;
        private readonly IRoleService _roleService;
        private readonly UserUtil userUtil = new UserUtil();

        public UserService(HotelDbContext repository, IRoleService roleService)
        {
            _repository = repository;
            _roleService = roleService;
        }

        public Task<bool> DeactivateUser(Guid userId)
        {
            throw new NotImplementedException();
        }

        public async Task<User?> GetUserByUsername(string username)
        {
            var user = await _repository.Users.SingleOrDefaultAsync(x => x.Username == username);
            return user;
        }

        public async Task<ResultCustom> Login(UserLoginCore model)
        {
            try
            {
                //encrypt password to compare with password in database
                var pass = userUtil.Encrypt(model.Password);
                var user = await _repository.Users.SingleOrDefaultAsync(x => x.Username == model.Username && x.Password == pass);
                if (user == null)
                {
                    return new ResultCustom()
                    {
                        Code = StatusCodes.Status400BadRequest,
                        Message = "Username or password is incorrect",
                    };
                }
                if (user.IsActive == false)
                {
                    return new ResultCustom()
                    {
                        Code = StatusCodes.Status400BadRequest,
                        Message = "User is inactive!",
                    };
                }
                return new ResultCustom()
                {
                    Code = StatusCodes.Status200OK,
                    Message = $"Login successfully! Welcome back: {user.FullName}",
                };
            }
            catch (Exception ex)
            {
                return new ResultCustom()
                {
                    Code = StatusCodes.Status400BadRequest,
                    Message = ex.Message,
                };
                throw;
            }
        }

        public async Task<ResultCustom> Register(UserRegisterCore model)
        {
            try
            {
                if (IsDuplicateUser(model))
                {
                    return new ResultCustom()
                    {
                        Code = StatusCodes.Status400BadRequest,
                        Message = "User is already exist, please check email, phonenumber or username",
                    };
                }
                var validateUser = ValidateUserRegister(model);
                if (validateUser.Code == 400)
                {
                    return new ResultCustom()
                    {
                        Code = StatusCodes.Status400BadRequest,
                        Message = validateUser.Message,
                    };
                }
                if (userUtil.IsArrivalWithDepartureDate(model.ArrivalDate, model.DepartureDate))
                {
                    return new ResultCustom()
                    {
                        Code = StatusCodes.Status400BadRequest,
                        Message = "Departure date must be later than arrival date! Date must be later or equal today!",
                    };
                }
                var roles = _roleService.GetAllRoles();
                var role = _roleService.GetRoleByTitle(model.Role);
                if (!roles.Contains(role.Result))
                {
                    return new ResultCustom()
                    {
                        Code = StatusCodes.Status400BadRequest,
                        Message = "Role does not exist!",
                    };
                }
                var user = new User()
                {
                    Id = Guid.NewGuid(),
                    FullName = model.FullName,
                    Email = model.Email,
                    PhoneNumber = model.PhoneNumber,
                    Username = model.UserName,
                    Password = userUtil.Encrypt(model.Password),
                    RoleId = role.Result.Id,
                    DateBirth = model.DateBirth,
                    ArrivalDate = model.ArrivalDate,
                    DepartureDate = model.DepartureDate,
                    Gender = model.Gender,
                    IsActive = false,
                };
                _repository.Users.Add(user);
                await _repository.SaveChangesAsync();
                return new ResultCustom()
                {
                    Code = StatusCodes.Status200OK,
                    Message = "Register successfully"
                };
            }
            catch (Exception ex)
            {
                return new ResultCustom()
                {
                    Code = StatusCodes.Status400BadRequest,
                    Message = ex.Message,
                };
                throw;
            }
        }

        #region private method
        private bool IsDuplicateUser(UserRegisterCore model)
        {
            try
            {
                var user = _repository.Users.SingleOrDefault(x => x.Username == model.UserName
                || x.Email == model.Email
                || x.PhoneNumber == model.PhoneNumber);
                if (user != null)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        private ResultCustom ValidateUserRegister(UserRegisterCore model)
        {
            try
            {
                if (userUtil.IsValidEmail(model.Email))
                {
                    return new ResultCustom()
                    {
                        Code = StatusCodes.Status400BadRequest,
                        Message = "Email is invalid!",
                    };
                }
                if (model.Password != model.RePassword)
                {
                    return new ResultCustom()
                    {
                        Code = StatusCodes.Status400BadRequest,
                        Message = "RePassword is not match with password!",
                    };
                }
                if (userUtil.IsValidPassword(model.Password))
                {
                    return new ResultCustom()
                    {
                        Code = StatusCodes.Status400BadRequest,
                        Message = "Password must have more than 8 characters and have at least one upper case and one lower case!",
                    };
                }
                if (userUtil.IsValidPhoneNumber(model.PhoneNumber))
                {
                    return new ResultCustom()
                    {
                        Code = StatusCodes.Status400BadRequest,
                        Message = "Phone number must have 10 numbers and start with 0!",
                    };
                }
                if (model.DateBirth >= DateTime.Now)
                {
                    return new ResultCustom()
                    {
                        Code = StatusCodes.Status400BadRequest,
                        Message = "Date birth cannot be later than today!",
                    };
                }
                return new ResultCustom()
                {
                    Code = StatusCodes.Status200OK,
                    Message = "User is valid",
                };
            }
            catch (Exception ex)
            {
                return new ResultCustom()
                {
                    Code = StatusCodes.Status400BadRequest,
                    Message = ex.Message,
                };
                throw;
            }
        }
        #endregion
    }
}
