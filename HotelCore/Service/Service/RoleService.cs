﻿using HotelCore.Common;
using HotelCore.Repository.DataContext;
using HotelCore.Repository.Entity;
using HotelCore.Service.Interface;
using HotelCore.Service.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCore.Service.Service
{
    public class RoleService : IRoleService
    {
        private readonly HotelDbContext _repository;

        public RoleService(HotelDbContext repository)
        {
            _repository = repository;
        }
        public async Task<ResultCustom> AddNewRole(RoleModelCore roleModel)
        {
            try
            {
                if (IsDuplicateRole(roleModel.Title))
                {
                    return new ResultCustom()
                    {
                        Code = StatusCodes.Status400BadRequest,
                        Message = "Role is already exist!"
                    };
                }
                var role = new Role()
                {
                    Id = Guid.NewGuid(),
                    Title = roleModel.Title,
                    Description = roleModel.Description,
                };
                _repository.Roles.Add(role);
                await _repository.SaveChangesAsync();
                return new ResultCustom()
                {
                    Code = StatusCodes.Status200OK,
                    Message = "Add successfully!"
                };
            }
            catch (Exception ex)
            {
                return new ResultCustom()
                {
                    Code = StatusCodes.Status400BadRequest,
                    Message = ex.Message,
                };
                throw;
            }
        }

        public List<Role> GetAllRoles()
        {
            var roles = _repository.Roles.ToList();
            return roles;
        }

        public async Task<Role?> GetRoleByTitle(RoleType title)
        {
            var role = await _repository.Roles.SingleOrDefaultAsync(x => x.Title == title);
            return role;
        }

        public async Task<ResultCustom> Update(Guid roleId, RoleModelCore item)
        {
            try
            {
                var entity = _repository.Roles.SingleOrDefault(x => x.Id == roleId);
                if (entity == null)
                {
                    return new ResultCustom()
                    {
                        Code = StatusCodes.Status404NotFound,
                        Message = "Cannot find this role!",
                    };
                }
                entity.Title = item.Title;
                entity.Description = item.Description;
                _repository.Roles.Update(entity);
                await _repository.SaveChangesAsync();
                return new ResultCustom()
                {
                    Code = StatusCodes.Status200OK,
                    Message = "update successfully"
                };
            }
            catch (Exception ex)
            {
                return new ResultCustom()
                {
                    Code = StatusCodes.Status400BadRequest,
                    Message = ex.Message
                };
                throw;
            }
        }

        #region private method
        public bool IsDuplicateRole(RoleType type)
        {
            var role = _repository.Roles.SingleOrDefault(x => x.Title == type);
            if (role != null) return true;
            return false;
        }
        #endregion

    }
}
