﻿using AutoMapper;
using HotelAPI.Model;
using HotelCore.Service.Interface;
using HotelCore.Service.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HotelAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService _roleService;
        private readonly log4net.ILog _logger = log4net.LogManager.GetLogger(typeof(RoleController));
        private readonly IMapper _map;
        public RoleController(IRoleService roleService, IMapper map)
        {
            _roleService = roleService;
            _map = map;
        }
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var result = _roleService.GetAllRoles();
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.Error("Expection when get all roles " + ex.Message);
                return BadRequest(ex.Message);
                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> Add(RoleModel model)
        {
            try
            {
                var item = _map.Map<RoleModelCore>(model);
                var result = await _roleService.AddNewRole(item);
                if (result.Code == 400)
                {
                    return Ok(result.Message);
                }
                return Ok(item);
            }
            catch (Exception ex)
            {
                _logger.Error("anananana " + ex.Message);
                return BadRequest(ex.Message);
                throw;
            }
        }

        [HttpPut("/update")]
        public async Task<IActionResult> Update(Guid roleId, [FromBody] RoleModel model)
        {
            try
            {
                var item = _map.Map<RoleModelCore>(model);
                var result = await _roleService.Update(roleId, item);
                if (result.Code != 200)
                {
                    return Ok(result.Message);
                }
                return Ok(item);
            }
            catch (Exception ex)
            {
                _logger.Error("alahablaka " + ex.Message);
                return BadRequest(ex.Message);
                throw;
            }
        }
    }
}
