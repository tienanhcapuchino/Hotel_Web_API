﻿using AutoMapper;
using HotelAPI.Model;
using HotelCore.Service.Interface;
using HotelCore.Service.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HotelAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly log4net.ILog _logger = log4net.LogManager.GetLogger(typeof(UserController));
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        public UserController(IUserService userService, IMapper map)
        {
            _userService = userService;
            _mapper = map;
        }

        [AllowAnonymous]
        [HttpPost("/login")]
        public async Task<IActionResult> Login([FromBody] UserLoginModel item)
        {
            try
            {
                var model = _mapper.Map<UserLoginCore>(item);
                var user = await _userService.Login(model);
                if (user.Code == 400)
                {
                    return BadRequest(user.Message);
                }
                return Ok(user.Message);
            }
            catch (Exception ex)
            {
                _logger.Error(" whe jjhjbnn login user: " + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("/register")]
        public async Task<IActionResult> Register([FromBody] UserRegisterModel model)
        {
            try
            {
                var item = _mapper.Map<UserRegisterCore>(model);
                var result = await _userService.Register(item);
                if (result.Code == 400)
                {
                    return BadRequest(result.Message);
                }
                return Ok(result.Message);
            }
            catch (Exception ex)
            {
                _logger.Error($"Could not register {ex.Message}");
                return BadRequest($"Could not register {ex.Message}");
                throw;
            }
        }
    }
}
