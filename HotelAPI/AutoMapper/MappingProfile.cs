﻿using AutoMapper;
using HotelAPI.Model;
using HotelCore.Service.Model;

namespace HotelAPI.AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<UserLoginModel, UserLoginCore>();
            CreateMap<RoleModel, RoleModelCore>();
            CreateMap<UserRegisterModel,UserRegisterCore>();
        }
    }
}
