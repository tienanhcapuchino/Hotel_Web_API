﻿using HotelCore.Repository.Entity;

namespace HotelAPI.Model
{
    public class UserModel
    {

    }
    public class UserLoginModel
    {
        public string? Username { get; set; }
        public string? Password { get; set; }
    }
    public class UserRegisterModel
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool Gender { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string RePassword { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime DepartureDate { get; set; }
        public RoleType Role { get; set; }
        public DateTime DateBirth { get; set; }
    }
}
